// 引入express模块
const express = require('express');

// 创建app对象
const app = express();

// 引入路由器模块
const router = require('./router')
// 配置模板资源目录
app.set('views','./06.公众号中开发网页/views');
// 配置模板引擎
app.set('view engine','ejs');

// 应用路由器
app.use(router);
// 监听端口号
app.listen(3000,()=>{
    console.log( "服务器启动成功！" )
})