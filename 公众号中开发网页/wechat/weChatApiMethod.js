/**
 * 获取access_token:
 *    微信调用接口全局唯一凭证
 * 
 * 特点：
 *  1. 唯一的
 *  2. 有效期为2小时,提前5分钟请求
 *  3. 接口限制，每天2000次
 * 
 * 请求的地址
 *      https请求方式: GET https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
 * 请求方式：
 *      Get
 * 思路：
 *   读取本地文件(定义方法ReadAccessToken(){})
 *       -本地有文件
 *          -判断是否过期(isValidAccessToken(){})
 *              -过期了
 *                  -重新获取access_token(getAccessToken(){})，保存下来覆盖之前的文件(保证文件是唯一的)(saveAccessToken(){})
 *              -没有过期
 *                  -直接使用
 *        -本地没有文件
 *          -发送请求获取access_token(getAccessToken(){})，保存下来(本地文件)(saveAccessToken(){})，直接使用
 */
// 引入request-promise-native即可
const rq = require('request-promise-native');

// 引入config模块
const {appID, appsecret} = require('../config');

// 引入menu模块
const menu = require('./menu');

// 引入网址前缀
const api = require('../untils/api');

// 引入封装好的文件保存模块
const {writeFileAsync,readFileAsync} = require('../untils/tool')

//  定义一个类
 class Wechat {
     constructor(){

     }
    /**
     * 用来获取access_token的方法
     */  
    // 1.用来获取access_token
    getAccessToken(){
        //  定义请求地址
        const url = api.accessToken+'&appid='+appID+'&secret='+appsecret;
        /**
         *  发送请求
         * 需要两个npm库 
         *      request
         *      request-promise-native 用来发送请求，并包装的promise对象,所以返回值是promise对象
         */

        return new Promise((resolve,reject)=>{
            rq( {method:'Get', url , json:true} )
            .then(res=>{
                // console.log( res )
                /**
                 * 拿到的数据：{access_token: "38_aB-4qmpFT_fuH7fmgvLg2rsM_MWdCwZNEaRdC37Tf-YVJlG…", expires_in: 7200}
                 */
                // 设置过期时间
                res.expires_in = Date.now() + (res.expires_in - 3000) * 1000;
                //成功后返回值
                resolve(res)
            })
            .catch(err=>{
                //失败后返回值
                reject("getAccessToken出现问题："+err)
            })
        })

    }

    /**
     * 2.用来保存access_token的方法
     */  
    saveAccessToken(accessToken){
      return writeFileAsync( accessToken,"access_token.txt" );
    }

    /**
     * 3.用来读取access_token的方法
     */ 
    readAccessToken(){
       return readFileAsync("access_token.txt");
    }

    /**
     * 4.判断日期是否过期access_token的方法
     */ 
    isValidAccessToken(data){
        // 检查传入的参数是否有效
        if(!data && !data.access_token && !data.expires_in ){
            // 代表access_token是无效的
            return false;
        }
        // // 检查access_token是否过期
        // if( data.expires_in < Data.now() ){
        //     // 过期了
        //     return false
        // }else{
        //     // 没有过期
        //     return true
        // }
        return data.expires_in > Date.now()
    }

    /**
     * 5.获取没有过期的access_token的方法
     * 
     */ 
    fetchAccesssToken(){
        if( this.access_token && this.expires_in && this.isValidAccessToken(this) ){
            // 以前有保存过的日期，日期不过期的，直接返回使用
            return Promise.resolve({
                access_token : this.access_token,
                expires_in : this.expires_in
            })
        }
        // return new Promise((resolve,reject)=>{
           return this.readAccessToken()
            .then(async res=>{
                // 本地有文件
                // 判断日期是否过期
                if( this.isValidAccessToken(res) ){
                    // 没有过期
                    return new Promise.resolve(res)
                    // resolve(res)

                }else{
                    // 过期了
                    // 再请求获取access_token
                    // 使用await方法，就不用.then方法了
                    const res = await this.getAccessToken()
                    // .then( res =>{
                        // 保存access_token存储到本地
                        await this.saveAccessToken(res)
                        // .then(res=>{
                            // 存储成功，直接使用,如何拿到返回值，外层嵌套promise,返回resolve
                            // resolve(res)
                            return new Promise.resolve(res)
                        // })
                    // })
                }
                })
            .catch(async err=>{
                // 本地没有文件，需要重新发送access_token请求
                const res = await this.getAccessToken();
                // .then( res =>{
                    // 存储access_token存储到本地
                    await this.saveAccessToken(res);
                    // .then(res=>{
                        // 存储成功，直接使用,如何拿到返回值，外层嵌套promise,返回resolve
                        // resolve(res)
                        return Promise.resolve(res)
                    // })
                // })
            })
            .then(res => {
                // 将access_token挂载到this上
                this.access_token = res.access_token;
                this.expires_in = res.expires_in;
                // 返回的res包装一层promise对象，返回成功的状态
                // 是最终返回值
                return Promise.resolve(res);
            })
        // })
    }

    /**
     * 用来获取JS_SDK(jsapi_ticket)的方法
     */  
    // 1.用来获取JS_SDK
    getTicket(){
        /**
         *  发送请求
         * 需要两个npm库 
         *      request
         *      request-promise-native 用来发送请求，并包装的promise对象,所以返回值是promise对象
         */
        return new Promise( async (resolve,reject)=>{
            // 获取accessToken 
            const token = await this.fetchAccesssToken()
            //  定义请求地址
            const url = api.ticket + "access_token=" + token.access_token;
            // console.log( url )
            rq( {method:'Get', url , json:true} )
            .then(res=>{
                // console.log( res )
                // 设置过期时间
                res.expires_in = Date.now() + (res.expires_in - 3000) * 1000;
                //直接返回成功的值
                resolve({
                    ticket : res.ticket,
                    expires_in : res.expires_in
                });
                
            })
            .catch(err=>{
                //失败后返回值
                reject("getAccessToken出现问题："+err)
            })
        })

    }

   
    // 2.用来保存JS_SDK的方法  
    saveTicket(ticket){
       return writeFileAsync( ticket,"ticket.txt" );
    }

    // 3.用来读取JS_SDK的方法 
    readTicket(){
       return readFileAsync("ticket.txt");
    }

    
    // 4.判断日期是否过期JS_SDK的方法 
    isValidTicket(data){
        // 检查传入的参数是否有效
        if(!data && !data.ticket && !data.expires_in ){
            // 代表Ticket是无效的
            return false;
        }
        return data.expires_in > Date.now()
    }

    
    // 5.获取没有过期的JS_SDK的方法         
    fetchTicket(){
        if( this.ticket && this.ticket_expires_in && this.isValidTicket(this) ){
            // 以前有保存过的日期，日期不过期的，直接返回使用
            return Promise.resolve({
                ticket : this.ticket,
                expires_in : this.expires_in
            })
        }

        // return new Promise((resolve,reject)=>{
           return this.readTicket()
            .then(async res=>{
                // 本地有文件
                // 判断日期是否过期
                if( this.isValidTicket(res) ){
                    // 没有过期
                    return new Promise.resolve(res)
                    // resolve(res)

                }else{
                    // 过期了
                    // 再请求获取Ticket
                    // 使用await方法，就不用.then方法了
                    const res = await this.getTicket()
                    // .then( res =>{
                        // 保存Ticket存储到本地
                        await this.saveTicket(res)
                        // .then(res=>{
                            // 存储成功，直接使用,如何拿到返回值，外层嵌套promise,返回resolve
                            // resolve(res)
                            return new Promise.resolve(res)
                        // })
                    // })
                }
                })
            .catch(async err=>{
                // 本地没有文件，需要重新发送Ticket请求
                const res = await this.getTicket();
                // .then( res =>{
                    // 存储access_token存储到本地
                    await this.saveTicket(res);
                    // .then(res=>{
                        // 存储成功，直接使用,如何拿到返回值，外层嵌套promise,返回resolve
                        // resolve(res)
                        return Promise.resolve(res)
                    // })
                // })
            })
            .then(res => {
                // 将Ticket挂载到this上
                this.ticket = res.ticket;
                this.ticket_expires_in = res.expires_in;
                // 返回的res包装一层promise对象，返回成功的状态
                // 是最终返回值
                return Promise.resolve(res);
            })
        // })
    }


    /**
     * 用来自定义菜单的方法
     */ 
    // 创建菜单
    creatMenu(menu){
        return new Promise( async (resolve,reject)=>{
            // 如何显示异常报错，async必须使用try catch的方法
            try{
                const data = await this.fetchAccesssToken();
                // console.log( "111"+token.access_token )
                // 定义请求地址
                const url = api.menu.create+"access_token="+data.access_token
                const result = await rq({method:'POST',url,json:true,body:menu})
                resolve( result )
            }catch(e){
                reject( "creatMenu方法异常："+ e )
            }
        })
    }

    // 创建前先删除，删除菜单方法：
    deleteMenu(){
        return new Promise( async (resolve,reject) => {
            try {
                const data = await this.fetchAccesssToken();
                // 定义请求地址
                const url = api.menu.delete+"access_token="+data.access_token
                // 发送请求
                const result = await rq( { method:"GET", url, json:true } )
                resolve( result );
            } catch (e) {
                reject( 'deleteMenu出了问题：'+e )
            }
        })
        
    }
    /**
     * 用来获取关注人信息的方法
     */ 
    getUserInfo(){
        return new Promise( async (resolve,reject)=>{
            try{
                const data = await this.fetchAccesssToken();
                const url ="https://api.weixin.qq.com/cgi-bin/user/get?access_token="+data.access_token+"&next_openid="
                const userResult = await rq({method:'GET',url,json:true})
                resolve(userResult.data.openid )
            }catch(e){
                reject( "getUserInfo方法异常:" + e )
            }
        })
    }

}

// 如果修改自定义菜单，需要执行以下这个方法
// (async()=>{
//     //  模拟测试
//     const we = new Wechat();
//     // 删除菜单，在创建
//     let result = await we.deleteMenu()
//     // console.log( "shanchu:"+ result.errmsg )
//     result = await we.creatMenu(menu)
//     // console.log( "chaungjian:"+ result.errcode )
//     // console.log( "chaungjian:"+ result.errmsg )
//     console.log(  result )
//     // const AccesssToken = await we.fetchAccesssToken();
//     // const Ticket = await we.fetchTicket();
//     // const UserInfo = await we.getUserInfo();
//     // console.log(  UserInfo )
//     // console.log(  UserInfo.data.openid )
// })()

module.exports = Wechat;


