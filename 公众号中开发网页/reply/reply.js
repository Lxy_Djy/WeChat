// 处理用户发送的信息内容判断
module.exports = message => {
/**
 * 遇到以下情况，微信就会在公众号会话中，向用户下发系统提示，“该公众号暂时无法提供服务，请稍后再试！”：
 *      1.开发者在5S没有回应任何内容
 *      2.开发者回复了除了xml的异常数据，比如json，字符串，xml数据中有多余的空格等
 */
    let options = {
        toUserName : message.FromUserName,
        fromUserName : message.ToUserName,
        createTime : Date.now(),
        msgType :  message.MsgType
    };
    let content = "你在唱什么，说中文。";
    // 判断用户发送的消息是否是文本消息
    if( message.MsgType === "text" ){
        // options.msgType = "text";
        // 判断用户发送的内容具体是什么
        if( message.Content === "1" ){
            content = "你好"

        }else if( message.Content.match("爱") ){
            // 半匹配
            content = "我爱你！"

        }
    }
    // else if( message.MsgType === "image" ){
    //     // options.msgType = "image";
    //     options.mediaId = message.MediaId;

    // }else if( message.MsgType === "voice" ){
    //     // options.msgType = "voice";
    //     options.mediaId = message.MediaId;

    // }else if( message.MsgType === "video" ){
    //     // options.msgType = "video";
    //     options.mediaId = message.MediaId;

    // }else if( message.MsgType === "shortvideo" ){
    //     // options.msgType = "shortvideo";
    //     options.mediaId = message.MediaId;

    // }else if( message.MsgType === "location" ){
    //     // options.msgType = "location";
    //     content = "维度："+message.Location_X+";经度："+message.Location_Y+";地图的缩放大小："+message.Scale+";具体位置信息："+message.Label;
    //     console.log( content )

    // }
    else if( message.MsgType === "event" ){
       if(message.Event === "subscribe"){
           //订阅成功    
           content = "欢迎关注Darryl的公众号~ \n"+
            "回复 首页"

           if( message.EventKey ){
               content = "用户扫描带参数的二维码关注事件"

           }
       }else if ( message.Event === "unsubscribe" ){
           //取消订阅
           content = "无情取关！"

       }else if( message.Event === "CLICK" ){
           content = "您可以按照以下提示来进行操作 \n"
       }
    //     else if( message.Event === "SCAN" ){
    //         content = "用户已经关注过，再扫描带参数的二维码关注事件";

    //    }else if( message.Event === "LOCATION" ){
    //         content = "维度："+message.Latitude+";经度："+message.Longitude+";精度："+message.Precision;

    //    }
    }

    options.content = content
    return options
    
}