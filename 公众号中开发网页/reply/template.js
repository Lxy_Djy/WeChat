// 回复消息的模板

module.exports = options =>{ 

    let replyMessages = 
    `<xml>
        <ToUserName><![CDATA[`+options.toUserName+`]]></ToUserName>
        <FromUserName><![CDATA[`+options.fromUserName+`]]></FromUserName>
        <CreateTime>"+`+options.createTime+`+"</CreateTime>
        <MsgType><![CDATA[`+options.msgType+`]]></MsgType>`

    if( options.msgType === "text" ){

        replyMessages += `<Content><![CDATA[`+options.content+`]]></Content>`

    }else if( options.msgType === "image" ){

        replyMessages += `<Image><MediaId><![CDATA[`+options.mediaId+`]]></MediaId></Image>`

    }else if( options.msgType === "voice" ){

        replyMessages += `<Voice><MediaId><![CDATA[`+options.mediaId+`]]></MediaId></Voice>`
        
    }else if( options.msgType === "video" ){
        replyMessages += 
        `<Video>
            <MediaId><![CDATA[`+options.mediaId+`]]></MediaId>
            <Title><![CDATA[`+options.title+`]]></Title>
            <Description><![CDATA[`+options.description+`]]></Description>
        </Video>`
        
    }else if( options.msgType === "music" ){

        replyMessages += 
        `<Music>
            <Title><![CDATA[`+options.title+`]]></Title>
            <Description><![CDATA[`+options.description+`]]></Description>
            <MusicUrl><![CDATA[`+options.musicUrl+`]]></MusicUrl>
            <HQMusicUrl><![CDATA[`+options.hqMusicUrl+`]]></HQMusicUrl>
            <ThumbMediaId><![CDATA[`+options.mediaId+`]]></ThumbMediaId>
        </Music>`
        
    }else if( options.msgType === "news" ){

        replyMessages += 
           `<ArticleCount>`+options.content.length+`</ArticleCount>
            <Articles>`
            // 这个需要循环用户的列表item，有几个显示几个
            options.content.forEach(item => {
                replyMessages += 
                `<item>
                <Title><![CDATA[`+item.title+`]]></Title>
                <Description><![CDATA[`+item.description+`]]></Description>
                <PicUrl><![CDATA[`+item.picUrl+`]]></PicUrl>
                <Url><![CDATA[`+item.url+`]]></Url>
                </item>`
            });

        replyMessages += `</Articles>`

    }
    
    replyMessages += `</xml>`
    return replyMessages
}