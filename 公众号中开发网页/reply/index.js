
// 引入sha1
const sha1 = require('sha1');

// 引入配置模块
const config = require('../config')

// 引入tool模块
const {getUserDataAsync,parseXMLAsync,formatMessage} = require("../untils/tool")

// 引入template模块，
const template = require( "./template" )

// 引入reply模块
const reply = require( "./reply" )

// 验证服务器有效性模块
module.exports = () => {
/**
 * 1.VX服务器要知道开发者服务器是哪个
 *  在测试号管理页面上填写url开发者服务器地址
 *     - 使用ngrok 内网穿透，将本地端口号开启的服务映射成外网可访问的地址：ngrok http 端口号
 *   填写token
 *         自定义，参与微信签名加密的参数
 * 2.开发者服务器 - 验证消息是否来自于微信服务器
 *      1.计算微信签名(signature)和微信传来的签名，相同就是微信服务器发送的消息，不同就不是。
 *          算法：
 *              1.将微信发出的三个参数(timestamp,nonce,token),按照字典(a~z 0~9)进行排序,组合在一起形成一个数组
 *              2.讲数组里所有参数拼接成一个字符串，进行sha1加密
 *              3.加密完成后生成signature，和VX进行比较
 *              如果一样，说明消息是来自于微信服务器，返回echostr给微信服务器。
 *              如果不一样，说明消息不是来自于微信服务器，返回一个err。
 */
    return async (req,res,next) => {
        // console.log( req.query )     
        /**
         * {    signature: "75172931a7ce8c25af90a229c362931915f58302",//微信的加密签名 
         *      echostr: "2028718569696508905", //微信随机字符串
         *      timestamp: "1603250505", //时间戳
         *      nonce: "1663076446"//随机数字
         * }
         */
        // 解构赋值
        const {timestamp,nonce,signature,echostr } = req.query;
        const { token } = config;
        const sha1Str = sha1( [timestamp,nonce,token].sort().join('') )
        // console.log( sha1Str )

        /**
         * 接收用户发送来的信息
         *  vx服务器会发送两种类型的消息发送给开发者服务器
         *     1.GET请求
         *        - 验证服务器的有效性
         *     2.POST请求
         *        - 微信服务器会讲用户发送的数据以POST的请求方式转发给开发者服务器
         * 
         */
            if(req.method === 'GET'){
                //3.加密完成后生成signature，和VX进行比较
                if(sha1Str === signature){
                    res.send(echostr)
                }else{
                    res.end("err")
                }
            }else if( req.method === 'POST' ){
                // - 微信服务器会讲用户发送的数据以POST的请求方式转发给开发者服务器
                // 验证消息来自VX服务器
                if(sha1Str !== signature){
                    // 不相等就说明不是来自微信服务器
                    res.end('err')
                }
                /**
                 * {nonce:"1250098203"
                    openid:"oXkXa6vHzrN2nWNz_zhNYYtmPtjQ"  //   VX用户的ID
                    signature:"f436a55f7cb4a2b08a5eb7fa17168df593cd672b"
                    timestamp:"1603345658"}响应了三次
                 */
                // console.log( res.query )

                // 接收请求体中的数据，流式数据,异步方法，需要加await
                const xmlData = await getUserDataAsync(req)
                // console.log( xmlData )
                /**  xmlData:
                *  <xml>
                *       <ToUserName><![CDATA[gh_3b5d90f89c77]]></ToUserName>    //开发者的ID
                        <FromUserName><![CDATA[oXkXa6vHzrN2nWNz_zhNYYtmPtjQ]]></FromUserName>  //用户的ID
                        <CreateTime>1603347989</CreateTime> //发送的时间戳
                        <MsgType><![CDATA[text]]></MsgType> //发送的消息类型
                        <Content><![CDATA[456]]></Content>  //发送的消息内容
                        <MsgId>22954426585833305</MsgId>    //发送的消息ID 微信服务器会默认保存三天，通过此ID三天内可以找到消息数据，三天外就拉闸
                   </xml>
                 */
                
                // 将XML数据解析问js对象,异步方法，需要加await
                const jsData = await parseXMLAsync(xmlData)
                // console.log( jsData )
                /* 格式化后的“jsData”：
                    Content:Array(1) ["1111"]
                    CreateTime:Array(1) ["1603348898"]
                    FromUserName:Array(1) ["oXkXa6vHzrN2nWNz_zhNYYtmPtjQ"]
                    MsgId:Array(1) ["22954437927759687"]
                    MsgType:Array(1) ["text"]
                    ToUserName:Array(1) ["gh_3b5d90f89c77"]
                */

                // 格式化js对象，提取想要的数据,同步方法，不需要加await
                const message = formatMessage(jsData);
                // console.log( message )

                /**
                 * 简单的自动回复，回复文本内容
                 * 一旦遇到以下情况，微信就会在公众号会话中，向用户下发系统提示，“该公众号暂时无法提供服务，请稍后再试！”：
                 *      1.开发者在5S没有回应任何内容
                 *      2.开发者回复了除了xml的异常数据，比如json，字符串，xml数据中有多余的空格等
                 */
                // 用户发送内容进行判断
                const options = reply(message)
                
                // console.log( options.content )
                // console.log( options.msgType )
                
                // 最终回复的数据内容
                // let replyMessage = "<xml><ToUserName><![CDATA["+message.FromUserName+"]]></ToUserName><FromUserName><![CDATA["+message.ToUserName+"]]></FromUserName><CreateTime>"+Date.now()+"</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA["+content+"]]></Content></xml>"
                const replyMessage = template( options )
                // console.log( replyMessage )
                /**
                 * 如果开发者服务器没有响应给微信服务器，微信服务器会发送三次请求过来
                 * 所以最好加上res.end("")返回空，这样就不会发送三次接口请求了
                 */
                // res.end('');
                // 返回响应给微信服务器
                // console.log( replyMessage )
                res.send(replyMessage)
                
            }else{
                res.end("err")
            }

    }
}