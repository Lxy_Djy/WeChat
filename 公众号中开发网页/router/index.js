// 引入express模块
const express = require('express')

// 引入sha1加密
const sha1 = require('sha1');

// 引入reply模块
const reply = require('../reply');

// 引入Wechat模块
const Wechat = require('../wechat/weChatApiMethod');

// 引入api
const {url} = require( '../config' )

// 获取Router
const Router = express.Router;
// 创建路由器对象
const router = new Router()

// 创建实例对象
const wechatApi = new Wechat();

// 页面路由
router.get('/search', async (req,res) => {
/**
 * 生成js-jdk使用的签名：
 *  组合参与签名的四个参数，jsapi_ticket(临时票据)，nonceStr: ''(必填，生成签名的随机串),timestamp(时间戳)，url(当前服务器地址)
 *  将其转换成字符串，以“&”拼接
 *  进行sha1加密，最终生成signature
 */
  
  //按步骤组成signature
  const noncestr =String(Math.random()).split('.')[1];
  const timestamp = Date.now();
  // 获取票据
  const {ticket} = await wechatApi.fetchTicket();
  // 获取用户openid
  const userId = await wechatApi.getUserInfo();

  const arr = [
      "jsapi_ticket="+ticket,
      "noncestr="+noncestr,
      "timestamp="+timestamp,
      "url="+url+"/search"
  ]
    
  const str = arr.sort().join("&");
  // 最终生成signature
  const signature = sha1(str);
//   console.log( signature )

  // 渲染页面,传入signature,timestamp,noncestr参数到页面
  res.render('search',{
    signature,
    timestamp,
    noncestr,
    userId
  })

})

// 验证服务器的有效性
router.use(reply());

// 暴露出去
module.exports = router;