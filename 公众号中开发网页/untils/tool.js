/**
 * 工具函数包
 */

//  xml2js中2代表to
const {parseString} = require('xml2js')

// 引入fs模块
const {writeFile,readFile} = require('fs');

// 引入path
const {resolve} = require('path');

module.exports = {
    // 获取用户发来的数据
    // 需要传递req来使用on的方法
    getUserDataAsync(req){
      let xmlData = "";
      return new Promise((resolve,reject) => {
        req.on('data',data => {
            // 当数据流传递过来时，会触发当前事件，会将数据流注入回调函数中
            // console.log( data );
            // 读取的数据是buffer，需要转化成字符串
            xmlData += data.toString()
        })
        .on('end',() => {
            // 当数据流接收完毕时，触发当前事件
            resolve(xmlData)
        })
      }) 
    },

    // xml转换方法借助一个库：xmltojs
    parseXMLAsync(xmlData){
        return new Promise((resolve,reject)=>{
              parseString(xmlData,{trim:true},(err,data) => {
                if (!err) {
                    resolve( data )
                }else{
                    reject("parseXMLAsync方法出了问题："+ err )
                }
            })
        })
      
    },

    // 格式化想要的数据
    formatMessage(jsData){
        let message={};
        // 只获取xml中的数据
        jsData = jsData.xml;
        // 判断数据是否是一个对象
        if( typeof jsData === 'object' ){
            // 遍历对象
            for(let key in jsData){
                // 获取属性值
                let value = jsData[key];
                //过滤掉空数组
                if( Array.isArray(value) && value.length>0 ) {
                    // 合法的数据放入message中
                    message[key] = value[0];
                }
            }
        }
        
        return message;
    },
    // 将文件写入本地
    writeFileAsync(data,fileName){
        // 将对象保存成json字符串才可以保存
        data = JSON.stringify(data);
        // 绝对路径
        const filepath = resolve(__dirname,fileName);
        // 将access_token保存到一个文件中,因为写入文件是异步方法，所以需要包层promise方法
        return new Promise((resolve,reject)=>{
            writeFile( filepath, data, (err)=>{
                if(!err){
                    console.log("文件保存成功") 
                    resolve()
                }else{
                    reject("writeFileAsync失败:"+err)
                }
            })
        })
    },
    // 读取本地文件
    readFileAsync(fileName){
        // 将access_token保存到一个文件中,因为写入文件是异步方法，所以需要包层promise方法
        // 绝对路径
        const filepath = resolve(__dirname,fileName);
        return new Promise((resolve,reject)=>{
            readFile( filepath, (err,data)=>{
                if(!err){
                    console.log("文件读取成功");
                    //将json字符串转化成对象
                    data = JSON.parse(data);
                    resolve(data)
                }else{
                    reject("readFileAsync失败:"+err)
                }
            })
        })
    },

}